const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const aboutUsModel = mongoose.model(
  "aboutUsDB",
  new Schema({
    title: {
      type: String
    },
    description: {
      type: String
    },
 })
);

module.exports = aboutUsModel;
