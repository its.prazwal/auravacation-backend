const aboutUsQuery = require("./aboutUs.query");

function insertInfo(req, res, next) {
  aboutUsQuery
    .insertAboutUs(req.body)
    .then(function (data) {
      res.status(200).json("Information Added Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllInfo(req, res, next) {
  aboutUsQuery
    .getAllAboutUs()
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateInfo(req, res, next) {
  aboutUsQuery
    .updateAboutUs(req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Information Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeInfo(req, res, next) {
  aboutUsQuery
    .removeAboutUs(req.body._id)
    .then(function (data) {
      res.status(200).json("Information Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertInfo,
  updateInfo,
  removeInfo,
  getAllInfo,
};
