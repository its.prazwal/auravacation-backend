const router = require('express').Router();

const aboutUsRoute = require("./aboutUs/aboutUs.route");
const legalDocsRoute = require('./legalDocuments/legalDocs.route');
const ourTeamRoute = require("./ourTeam/ourTeam.route");
const privacyPolicyRoute = require("./privacy-policy/privacyPolicy.route");
const whyCompanyRoute = require("./whyCompany/whyCompany.route");
const contactUsRoute = require('./contactUs/contactUs.route');

router.use("/aboutUs", aboutUsRoute);
router.use('/legalDocs',legalDocsRoute);
router.use("/ourTeam", ourTeamRoute);
router.use("/privacyPolicy", privacyPolicyRoute);
router.use("/whyCompany", whyCompanyRoute);
router.use('/contactUs', contactUsRoute);

module.exports = router;