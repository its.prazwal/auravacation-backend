module.exports = function(data1, data2) {
  if (data2.fullName) data1.fullName = data2.fullName;
  if (data2.userName) data1.userName = data2.userName;
  if (data2.emailId) data1.emailId = data2.emailId;
  if (data2.mobileNo) data1.mobileNo = data2.mobileNo;
  if (data2.passWord) data1.passWord = data2.passWord;
  if (data2.resetToken) data1.resetToken = data2.resetToken;
  if (data2.resetExpiry) data1.resetExpiry = data2.resetExpiry;
  return data1;
};
