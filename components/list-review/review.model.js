const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reviewModel = mongoose.model(
  "reviewDB",
  new Schema({
      customerDetail: {
          fullName: {type: String},
          country: {type: String},
          emailId: {type: String},
          phone: {type: Number},
      },
    packageId: {
      type: Schema.Types.ObjectId,
      ref: 'packageDB',
    },
    title: {
      type: String,
    },
    detail: {
      type: String,
    },
    rating: {
      type: Number,
      enum: ["1", "2", "3", "4", "5"],
    },
    publishedDate: { type: Date, default: Date.now() } ,
    displayOption: { type: Boolean, default: false },
  })
);

module.exports = reviewModel;
