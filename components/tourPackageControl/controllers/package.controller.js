const packageQuery = require("./../packageQuery");
const {fileRemover} = require('../../../middlewares/fileHandler');

function insertPackage(req, res, next) { 
  packageQuery
    .insert(req.body)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      next({
        status: 400,
        msg: "Package insertion failure.",
      });
    });
}

function showAllPackage(req, res, next) {
  packageQuery
    .showAll({})
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => next({ status: 400, msg: "package not found" }));
}

function searchPackage(req, res, next) {
  packageQuery
    .search(req.query)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) =>
      next({ status: 400, msg: "No Packages related to search found." })
    );
}

function showPackageById(req, res, next) {
  packageQuery
    .showById(req.params.id)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      next({ status: 400, msg: "package not found" });
    });
}

function updatePackage(req, res, next) {
  packageQuery
    .update(req.params.id, req.body)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      next(err);
    });
}

function removePackage(req, res, next) {
  packageQuery
    .remove(req.params.id)
    .then((data) => {
      if(data.image){
        fileRemover(`image/packageIMG/${data.image}`);
      }
      res.status(200).json("Package removed.");
    })
    .catch((err) => {
      next({ status: 400, msg: "Package not found." });
    });
}

function uploadImage(req, res, next){
  if (req.fileError) {
    next({
      msg: "invalid file format",
    });
  }
  if (req.file) {
    req.body.image = req.file.filename;
  }
  packageQuery.showById(req.params.id)
  .then(data => {
    if(!data) next({msg: 'Package not found'});
    if(data.image) fileRemover(`image/packageIMG/${data.image}`);
    packageQuery.update(req.params.id, req.body)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      next(err);
    });
  })
  .catch(err => next({msg: 'internal server error'}))
}

module.exports = {
  insertPackage,
  showAllPackage,
  showPackageById,
  updatePackage,
  removePackage,
  searchPackage,
  uploadImage,
};
