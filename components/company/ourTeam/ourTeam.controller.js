const ourTeamQuery = require("./ourTeam.query");
const {fileRemover} = require('./../../../middlewares/fileHandler');

function insertMember(req, res, next) {
    ourTeamQuery
        .insert(req.body)
        .then(function (data) {
            res.status(200).json("Information Added Successfully");
        })
        .catch(function (err) {
            next(err);
        });
}

function getAllMember(req, res, next) {
    ourTeamQuery
        .getAll()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function updateMember(req, res, next) {
    ourTeamQuery
        .update(req.body._id || req.params.id, req.body)
        .then(function (data) {
            res.status(200).json("Information Updated Successfully.");
        })
        .catch(function (err) {
            next(err);
        });
}

function removeMember(req, res, next) {
    ourTeamQuery.getAll()
        .then(function (data) {
            const member = data.filter(ele => JSON.stringify(ele._id) === JSON.stringify(req.body._id))[0];
            fileRemover(`/image/ourTeamIMG/${member.image}`);
            ourTeamQuery
                .remove(req.body._id)
                .then(function (data) {
                    res.status(200).json("Information Deleted.");
                })
                .catch(function (err) {
                    return next(err);
                });
        })
        .catch(function (err) {
            next({msg: 'error in removing image.'})
        });
}

function uploadImage(req, res, next) {
    if (req.fileTypeError) {
        return next({msg: 'Invalid file format'});
    }
    if (req.file) {
        ourTeamQuery.getAll()
            .then(function (data) {
                const member = data.filter(ele => JSON.stringify(ele._id) === JSON.stringify(req.params.id))[0];
                fileRemover(`image/ourTeamIMG/${member.image}`);
                ourTeamQuery.update(req.params.id, {image: req.file.filename})
                    .then(data => res.status(200).json('Image updated.'))
                    .catch(err => {
                        return next(err);
                    });
            })
            .catch(function (err) {
                next({msg: 'error in removing image.'})
            });
    }
}

module.exports = {
    insertMember,
    updateMember,
    removeMember,
    getAllMember,
    uploadImage
};
