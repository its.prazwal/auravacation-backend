const directMailQuery = require('./directMail.query');

function insertMail(req, res, next) {
    directMailQuery.insertMail({message: req.body.message, customerId: customer._id})
        .then(function (data) {
            res.status(200).json('Message sent successfully.')
        })
        .catch(function (err) {
            next(err)
        })
}

function getAllMail(req, res, next) {
    directMailQuery.getAllMail()
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })
}

function getTrashMail(req, res, next) {
    directMailQuery.getTrashMail()
        .then(function (data) {
            res.status(200).json(data)
        })
        .catch(function (err) {
            next(err)
        })
}

function updateMail(req, res, next) {
    directMailQuery.updateMail(req.params.id, req.body)
        .then(function (data) {
            res.status(200).json('Mail Updated.')
        })
        .catch(function (err) {
            next(err)
        })
}

function removeMail(req, res, next) {
    directMailQuery.removeMail(req.params.id)
        .then(function (data) {
            res.status(200).json('Message removed successfully.')
        })
        .catch(function (err) {
            next(err)
        })
}

module.exports = {
    getAllMail,
    getTrashMail,
    insertMail,
    updateMail,
    removeMail
}