const controller = require("./ourTeam.controller");
const router = require("express").Router();
const {uploadSingleImage} = require('../../../middlewares/fileHandler');
const authenticate = require('../../../middlewares/authenticate');
router.route("/")
    .get(controller.getAllMember)
    .post(authenticate, controller.insertMember)
    .put(authenticate, controller.updateMember)
    .delete(authenticate, controller.removeMember);

router.route("/upload/:id")
    .put(authenticate, uploadSingleImage('img', 'ourTeamIMG'), controller.uploadImage);

module.exports = router;
