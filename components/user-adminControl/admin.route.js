const router = require("express").Router();
const controller = require("./controllers/adminCtrl");
const authenticate = require("../../middlewares/authenticate");
const fileUpload = require("../../middlewares/fileHandler").upload;
const forgotPassword = require("./password-control/forgot-password");
const resetPassword = require("./password-control/reset-password");
const changePassword = require("./password-control/change-password");
const verifyPassword = require("./password-control/verify-password");

router
  .route("/register")
  .get(controller.checkExistingAdmin)
  .post(controller.insertAdmin);

router.route("/login").post(controller.loginAdmin);

router
  .route("/")
  .get(authenticate, controller.getUser)
  .put(authenticate, controller.updateAdmin);

router.route("/forgot-password").post(forgotPassword);

router.route("/reset-password/:token").put(resetPassword);

router.route("/verify-password").post(authenticate, verifyPassword);
router.route("/change-password").put(authenticate, verifyPassword, changePassword);

module.exports = router;
