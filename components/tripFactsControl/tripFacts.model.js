const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const countryModel = mongoose.model(
  "countryDB",
  new Schema({
    name: { type: String, required: true },
    detail: { type: String },
  })
);

const regionModel = mongoose.model(
  "regionDB",
  new Schema({
    name: { type: String, required: true },
    detail: { type: String },
    majorAttractions: { type: Array },
    visitSeasons: {
      seasons: { type: Array },
      briefing: { type: String },
    },
    otherInfo: { type: String },
  })
);

const placesModel = mongoose.model(
  "placesDB",
  new Schema({
    name: { type: String, required: true },
    detail: { type: String },
    majorAttractions: { type: Array },
  })
);

const styleModel = mongoose.model(
  "styleDB",
  new Schema({
    name: { type: String, required: true },
    detail: { type: String },
  })
);

const transportationModel = mongoose.model(
  "transportationDB",
  new Schema({
    name: { type: String },
  })
);

const accommodationModel = mongoose.model(
  "accommodationDB",
  new Schema({
    name: { type: String },
  })
);

const highlightsModel = mongoose.model(
  "highlightsDB",
  new Schema({
    statement: { type: String },
  })
);
const usefulInfoModel = mongoose.model(
  "usefulInfoDB",
  new Schema({
    title: { type: String },
    description: { type: String },
  })
);

module.exports = {
  countryModel,
  regionModel,
  placesModel,
  styleModel,
  highlightsModel,
  transportationModel,
  accommodationModel,
  usefulInfoModel,
};
