module.exports = function privacyPolicyMap(data1, data2) {
  if (data2.sortIndex) data1.sortIndex = data2.sortIndex;
  if (data2.title) data1.title = data2.title;
  if (data2.description) data1.description = data2.description;
  return data1;
};
