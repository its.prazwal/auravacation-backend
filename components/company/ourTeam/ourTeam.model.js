const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ourTeamModel = mongoose.model(
  "ourTeamDB",
  new Schema({
    sortIndex: {
      type: Number,
    },
    fullName: {
      type: String,
      required: true,
    },
    designation: {
      type: String,
      required: true,
    },
    image: {
      type: String,
    },
    message: {
      type: String,
    },
  })
);

module.exports = ourTeamModel;
