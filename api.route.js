const router = require("express").Router();

const adminRoute = require("./components/user-adminControl/admin.route");
const advActRoute = require("./components/list-advAct/advAct.route");
const bookingRoute = require('./components/booking/booking.route');
const costDetailsRoute = require("./components/list-costDetails/costDetail.route");
const companyRoute = require('./components/company/company.route');
const directMailRoute = require('./components/directMail/directMail.route');
const homepageRoute = require('./components/homepage/homepage.route');
const packageRoute = require("./components/tourPackageControl/packageRoute");
const reviewRoute = require("./components/list-review/review.route");
const tripFactsRoute = require("./components/tripFactsControl/tripFacts.route");
const subscribedRoute = require('./components/subscribed/subscribed.route');
const extLinksRoute = require('./components/externalLinks/extLinks.route');

router.use("/adventureAct", advActRoute);
router.use('/booking', bookingRoute);
router.use('/company', companyRoute)
router.use("/costDetails", costDetailsRoute);
router.use('/directMail', directMailRoute);
router.use('/extLinks', extLinksRoute)
router.use('/homepage', homepageRoute);
router.use("/package", packageRoute);
router.use("/review", reviewRoute);
router.use('/subscribed', subscribedRoute);
router.use("/tripFacts", tripFactsRoute);
router.use("/user", adminRoute);

module.exports = router;
