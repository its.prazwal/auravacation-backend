const router = require('express').Router();
const controller = require('./contactUs.controller');
const authenticate = require('../../../middlewares/authenticate');

router.route('/')
.get(controller.getAllContact)
.post(authenticate, controller.insertContact)
.put(authenticate, controller.updateContact)
.delete(authenticate, controller.removeContact);

router.route('/:id')
.get(controller.getContactById)
.put(authenticate, controller.updateContact)
.delete(authenticate, controller.removeContact);

module.exports = router;