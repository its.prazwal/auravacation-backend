const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const privacyPolicyModel = mongoose.model(
  "privacyPolicyDB",
  new Schema({
    sortIndex: {
      type: Number,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    }
  })
);

module.exports = privacyPolicyModel;
