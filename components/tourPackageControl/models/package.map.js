module.exports = function (data1, data2) {
  if (data2.country) data1.country = data2.country;
  if (data2.style) data1.style = data2.style;
  if (data2.packageName) data1.packageName = data2.packageName;
  if (data2.region) data1.region = data2.region;
  if (data2.advType) data1.advType = data2.advType;
  if (data2.tripFacts) {
    if (data2.tripFacts.duration)
      data1.tripFacts.duration = data2.tripFacts.duration;
    if (data2.tripFacts.diffculty)
      data1.tripFacts.diffculty = data2.tripFacts.diffculty;
    if (data2.tripFacts.transportation)
      data1.tripFacts.transportation = data2.tripFacts.transportation;
    if (data2.tripFacts.accommodation)
      data1.tripFacts.accommodation = data2.tripFacts.accommodation;
    if (data2.tripFacts.price)
      data1.tripFacts.price = data2.tripFacts.price;
    if (data2.tripFacts.meals)
      data1.tripFacts.meals = data2.tripFacts.meals;
    if (data2.tripFacts.altitude)
      data1.tripFacts.altitude = data2.tripFacts.altitude;
    if (data2.tripFacts.season)
      data1.tripFacts.season = data2.tripFacts.season;
  }
  if (data2.highlights) data1.highlights = data2.highlights;
  if (data2.description) data1.description = data2.description;
  if (data2.image) data1.image = data2.image;
  if (data2.itinerary) data1.itinerary = data2.itinerary;
  if (data2.costDetails) {
    if (data2.costDetails.includes)
      data1.costDetails.includes = data2.costDetails.includes;
    if (data2.costDetails.excludes)
      data1.costDetails.excludes = data2.costDetails.excludes;
  }
  if (data2.displayOption) {
    data1.displayOption = data2.displayOption;
  } else {
    data1.displayOption = data2.displayOption;
  }
  return data1;
};
