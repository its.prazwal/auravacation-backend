const legalDocsModel = require("./legalDocs.model");

function insert(datas) {
  return legalDocsModel(datas).save(datas);
}

function getAll() {
  return legalDocsModel.find();
}

function remove(id) {
  return legalDocsModel.findByIdAndDelete(id);
}

module.exports = {
  insert,
  getAll,
  remove,
};
