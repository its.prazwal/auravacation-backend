const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let directMailModel = mongoose.model('directMailDB', new Schema({
    customerDetail: {
        fullName: {type: String},
        country: {type: String},
        emailId: {type: String},
        phone: {type: Number},
    },
    message: {type: String},
    packageId: {type: Schema.Types.ObjectId, ref: 'packageDB'},
    receivedDate: {type: Date, default: Date.now()},
    read: {type: Boolean, default: false},
    deleted: {type: Boolean, default: false},
    replied: {type: Boolean, default: false},
}))

module.exports = directMailModel;
