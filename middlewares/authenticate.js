let jwt = require("jsonwebtoken");
let config = require("./../config/index");
let adminModel = require("./../components/user-adminControl/models/adminModel");

module.exports = function (req, res, next) {
  let token;
  if (req.headers["x-access-token"]) {
    token = req.headers["x-access-token"];
  } else if (req.headers["authorization"]) {
    token = req.headers["authorization"];
  } else if (req.headers["token"]) {
    token = req.headers["token"];
  } else {
    next({ msg: "Token not found" });
  }
  if (token) {
    jwt.verify(token, config.jwtSecret, function (err, logIn) {
      if (err) {
        return next({
          msg: err.expiredAt ? 'Login Session Expired': "unauthorized access",
          status: err.expiredAt ? 440 : 401,
        });
      }
      adminModel.findById(logIn.id).exec(function (err, user) {
        if (err) {
          return next(err);
        }
        if (!user) {
          return next({
            msg: "user removed from system",
          });
        }
        req.userId = user._id;
        next();
      });
    });
  } else {
    next({ msg: "Unauthorized Token" });
  }
};
