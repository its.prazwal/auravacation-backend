const bookingQuery = require('./booking.query');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: "Gmail",
    secure: true,
    auth: {
        user: "site.testbypz@gmail.com",
        pass: "mepraz20",
    },
});

function prepareMail(data) {
    return {
        from: "Aura Vacation <noreply@auravacation.com>",
        to: data.emailId,
        subject: "Booking Confirmation",
        html: `<p>Hi, <strong>${data.fullName}</strong></p>
          <p>Kindly have this confirmation code for your booking from Aura Vacation:</p>
          </br>
          <h1>${data.bookingId}</h1>
        <p>This confirmation code will be use to verify your booking for further processes.</p>
          <p>Thank You</p>
          <p>Aura Vacation Pvt. Ltd.</p>`,
    }
}

function insertBooking(req, res, next) {
    bookingQuery.insertBooking(req.body)
        .then(function (data) {
            sender.sendMail(prepareMail({
                emailId: data.emailId,
                fullName: data.fullName,
                bookingId: data._id
            }), function (err, done) {
                if (err) {
                    return next({
                        status: 400,
                        msg: "Email Sending Failure",
                        err,
                    });
                }
                ;
                res.status(200).json({
                    msg: 'Package booked successfully',
                    bookingId: data._id
                });
            })
        })
        .catch(function (err) {
            next(err);
        })
}

function getAllBooking(req, res, next) {
    bookingQuery.getAllBooking()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function getTrashBooking(req, res, next) {
    bookingQuery.getTrashBooking()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function getBookingById(req, res, next) {
    bookingQuery.getBookingById(req.params.id)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function updateBooking(req, res, next) {
    bookingQuery.updateBooking(req.params.id, req.body)
        .then(function (data) {
            res.status(200).json('Booking updated successfully.');
        })
        .catch(function (err) {
            next(err);
        })
}

function removeBooking(req, res, next) {
    bookingQuery.removeBooking(req.params.id)
        .then(function (data) {
            res.status(200).json('Booking deleted successfully.');
        })
        .catch(function (err) {
            next(err);
        })
}


module.exports = {
    getAllBooking,
    getBookingById,
    getTrashBooking,
    insertBooking,
    removeBooking,
    updateBooking,
}