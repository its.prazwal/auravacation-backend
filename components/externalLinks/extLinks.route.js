const controller = require("./extLinks.controller");
const router = require("express").Router();
const authenticate = require("../../middlewares/authenticate");

router.route("/")
.get(controller.getAllExtLink)
.post(authenticate, controller.insertExtLink)
.put(authenticate, controller.updateExtLink)
.delete(authenticate, controller.removeExtLink);

module.exports = router;
