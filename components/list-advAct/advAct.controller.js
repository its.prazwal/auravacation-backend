const advActQuery = require("./advAct.query");
const {fileRemover} = require('./../../middlewares/fileHandler');

function insertInfo(req, res, next) {
  advActQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("Information Added Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllInfo(req, res, next) {
  advActQuery
    .getAll()
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateInfo(req, res, next) {
  advActQuery
    .update(req.body._id , req.body)
    .then(function (data) {
      res.status(200).json("Information Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeInfo(req, res, next) {
  advActQuery
    .remove(req.body._id)
    .then(function (data) {
      if(data.image){
      fileRemover(`image/advActIMG/${data.image}`);
      }
      res.status(200).json("Information Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}

function uploadImage(req, res, next) {
  if (req.fileTypeError) {
      return next({msg: 'Invalid file format'});
  }
  if (req.file) {
      advActQuery.getAll()
          .then(function (data) {
              const advAct = data.filter(ele => JSON.stringify(ele._id) === JSON.stringify(req.params.id))[0];
              if(advAct.image){               
              fileRemover(`image/advActIMG/${advAct.image}`);
              }
              advActQuery.update(req.params.id, {image: req.file.filename})
                  .then(data => res.status(200).json('Image updated.'))
                  .catch(err => {
                      return next(err);
                  });
          })
          .catch(function (err) {
              next({msg: 'error in removing image.'})
          });
  }
}

module.exports = {
  insertInfo,
  updateInfo,
  removeInfo,
  getAllInfo,
  uploadImage,
};
