const router = require("express").Router();
const controller = require("./costDetails.controller");
const authenticate = require("../../middlewares/authenticate");

router
  .route("/includes")
  .get(controller.getAllIncludes)
  .post(authenticate, controller.insertIncludes)
  .put(authenticate, controller.updateIncludes)
  .delete(authenticate, controller.removeIncludes);
router
  .route("/excludes")
  .get(controller.getAllExcludes)
  .post(authenticate, controller.insertExcludes)
  .put(authenticate, controller.updateExcludes)
  .delete(authenticate, controller.removeExcludes);
module.exports = router;
