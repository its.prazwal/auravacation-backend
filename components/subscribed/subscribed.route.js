const router = require('express').Router();

const authenticate = require('./../../middlewares/authenticate');
const controller = require('./subscribed.controller');

router.route('/')
.get(authenticate, controller.getAllMails)
.post(controller.insertNew);

module.exports = router;