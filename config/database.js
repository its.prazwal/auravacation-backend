const mongoose = require("mongoose");

const dbname = "travelSite";

//MONGO DB CLOUD SERVER
const cnxurl = "mongodb+srv://cluster0-jluxz.mongodb.net";
const dbOptions = {
  user: 'travel-site',
  pass: 'mepraz13'
}

//LOCALHOST SERVER
// const cnxurl = 'mongodb://localhost:27017';

mongoose.set("useNewUrlParser", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect(cnxurl + "/" + dbname, dbOptions, function (err, done) {
  if (err) {
    console.log("Database connection unsuccessful.");
  } else {
    console.log("Database *" + dbname + "* has connected.");
  }
});
