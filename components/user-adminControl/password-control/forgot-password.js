const adminQuery = require("../query/adminQuery");
const randomSt = require("randomstring");
const nodemailer = require("nodemailer");

const sender = nodemailer.createTransport({
  service: "Gmail",
  secure: true,
  auth: {
    user: "site.testbypz@gmail.com",
    pass: "mepraz20",
  },
});

function prepareMail(data) {
  return (mailContent = {
    from: "Auravacation <noreply@auravacation.com>",
    to: data.emailId,
    subject: "Forgot Password",
    html: `<p>Hi, <strong>${data.fullName}</strong></p>
        <p>We have received a request that you want to reset your password.</p>
        </br>
        <p><a href='${data.link}'>Reset Password >></a></p>
        <p>This link will expire in 24 hours and can be used only once.</p>
        </br>
        <p>If you didn't request this mail, please ignore and delete this message.</p>
        <p>Thank You</p>
        <p>Aura Vacation Pvt. Ltd.</p>`,
  });
}

function forgotPassword(req, res, next) {
  adminQuery
    .findOneAdmin(req.body)
    .then(function (user) {
      if (user) {
        const resetToken = randomSt.generate(20);
        const resetExpiry = new Date(Date.now() + 1000 * 60 * 60 * 24);
        let mailData = {
          fullName: user.fullName,
          emailId: user.emailId,
          link: `${req.headers.origin}/Reset-Password/${resetToken}`,
        };
        adminQuery
          .update(user._id, { resetExpiry, resetToken })
          .then(function (data) {
            sender.sendMail(prepareMail(mailData), function (err, done) {
              if (err) {
                return next({
                  status: 400,
                  msg: "Email Sending Failure",
                  err,
                });
              }
              res.json(done);
            });
          })
          .catch(function (err) {
            next(err);
          });
      } else {
        next({
          status: 404,
          msg: "Email Address not registered.",
        });
      }
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = forgotPassword;
