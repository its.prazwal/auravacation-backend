const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const packageModel = mongoose.model(
  "packageDB",
  new Schema({
    adventureType: { type: String },
    costDetails: {
      includes: { type: Array },
      excludes: { type: Array },
    },
    country: { type: String },
    description: { type: String },
    displayOption: { type: Boolean },
    highlights: { type: Array },
    image: { type: String },
    itinerary: [
      {
        day: { type: Number },
        title: { type: String },
        detail: { type: String },
      },
    ],
    packageName: { type: String },
    region: { type: String },
    reviewsList: { type: Array },
    style: { type: String },
    tripFacts: {
      accommodation: { type: Array },
      altitude: { type: Number },
      difficulty: { type: String },
      duration: { type: Number },
      meals: { type: Array },
      price: { type: Number },
      seasons: { type: Array },
      transportation: { type: Array },
    },
    usefulInfo: { type: Array },
  })
);

module.exports = packageModel;
