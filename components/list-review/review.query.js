const reviewModel = require("./review.model");

function insert(data) {
  return reviewModel(data).save();
}

function getAll() {
  return reviewModel.find({deleted: false});
}

function update(id, data) {
  return reviewModel.findByIdAndUpdate(id, data);
}

module.exports = {
  insert,
  getAll,
  update
};
