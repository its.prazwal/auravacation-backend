const adminModel = require("./../models/adminModel");
const passwordHash = require("password-hash");

function verifyPassword(req, res, next) {
  adminModel.findById(req.userId).exec(function (err, user) {
    if (err) {
      return next(err);
    }
    let isMatched = passwordHash.verify(req.body.prevPassword, user.passWord);
    if (!isMatched) {
      return next({ msg: "Password verification Failure." });
    } else {
      if(req.body.passWord){
      req.verified = true;
      next();
      }
      else{
      res.status(200).json("Password verified.");
      }
    }
  });
}

module.exports = verifyPassword;
