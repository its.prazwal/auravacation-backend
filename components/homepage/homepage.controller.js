const homepageQuery = require('./homepage.query');

function insertLayout(req, res, next){
    homepageQuery.getAllLayout()
    .then(function(saved){
        if(saved){
            const oldSelected = saved.filter(val => val.selected === true);
            if(oldSelected.length > 0){
                homepageQuery.updateLayout(oldSelected[0]._id, {selected: false})
                .then(function(updated){console.log('old selected remove.')})
                .catch(function(err){ next(err) });
            }
            homepageQuery.insertLayout(req.body)
                .then(function(data){
                    res.status(200).json('New layout saved.')
                })
                .catch(function(err){
                    next(err);
                })
        }
    })
    
}

function getAllLayout(req, res, next){
    homepageQuery.getAllLayout()
    .then(function(data){
        res.status(200).json(data);
    })
    .catch(function(err){
        next(err);
    })
}

function updateLayout(req, res, next){
    if(req.body.selected){
        homepageQuery.getAllLayout()
        .then(function(saved){
            if(saved){
                const oldSelected = saved.filter(val => val.selected === true);
                if(oldSelected.length > 0){
                    homepageQuery.updateLayout(oldSelected[0]._id, {selected: false})
                    .then(function(updated){console.log('old selected remove.')})
                    .catch(function(err){ next(err) });
                }
            }
        })
    }
    homepageQuery.updateLayout(req.params.id, req.body)
                .then(function(data){
                    res.status(200).json('Layout updated.')
                })
                .catch(function(err){
                    next(err);
                })
}

function removeLayout(req, res, next){
    homepageQuery.removeLayout(req.params.id)
    .then(function(data){
        res.status(200).json('Layout removed');
    })
    .catch(function(err){
        next(err);
    })
}

module.exports = {
    getAllLayout,
    insertLayout,
    updateLayout,
    removeLayout,
}