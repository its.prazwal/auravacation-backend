const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const includesModel = mongoose.model(
  "costIncludesDB",
  new Schema({
    statement: {
      type: String,
    },
  })
);

const excludesModel = mongoose.model(
  "costExcludesDB",
  new Schema({
    statement: {
      type: String,
    },
  })
);

module.exports = {
  includesModel,
  excludesModel,
};
