const router = require('express').Router();
const controller = require('./homepage.controller');
const authenticate = require('../../middlewares/authenticate');

router.route('/')
.get(controller.getAllLayout)
.post(authenticate, controller.insertLayout);

router.route('/:id')
.put(authenticate, controller.updateLayout)
.delete(authenticate, controller.removeLayout);

module.exports = router;