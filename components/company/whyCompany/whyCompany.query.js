const whyCompanyModel = require("./whyCompany.model");
const whyCompanyMap = require("./whyCompany.map");

function insert(datas) {
  const newData = new whyCompanyModel(datas);
  return whyCompanyMap(newData, datas).save();
}

function getAll() {
  return whyCompanyModel.find().sort({ sortIndex: 1 });
}

function update(id, data) {
  return whyCompanyModel.findByIdAndUpdate(id, data);
}

function remove(id) {
  return whyCompanyModel.findByIdAndRemove(id);
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
