const privacyPolicyQuery = require("./privacyPolicy.query");

function insertInfo(req, res, next) {
  privacyPolicyQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("Added new Privacy & Policy");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllInfo(req, res, next) {
  privacyPolicyQuery
    .getAll()
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateInfo(req, res, next) {
  privacyPolicyQuery
    .update(req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Information Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeInfo(req, res, next) {
  privacyPolicyQuery
    .remove(req.body._id)
    .then(function (data) {
      res.status(200).json("Information Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertInfo,
  updateInfo,
  removeInfo,
  getAllInfo,
};
