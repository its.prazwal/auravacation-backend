const advActModel = require("./advAct.model");

function insert(datas) {
  return advActModel(datas).save();
}

function getAll() {
  return advActModel.find();
}

function update(id, data) {
  return advActModel.findByIdAndUpdate(id, data);
}

function remove(id) {
  return advActModel.findByIdAndRemove(id);
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
