const subscribedModel = require('./subscribed.model');

function insert(data){
    return subscribedModel(data).save();
}

function getAll(){
    return subscribedModel.find();
}

module.exports = {
    insert,
    getAll,
}