const controller = require("./review.controller");
const router = require("express").Router();
const authenticate = require("../../middlewares/authenticate");

router
  .route("/")
  .get(controller.getAllReview)
  .post(authenticate, controller.insertReview);
router.route("/:id")
    .put(authenticate, controller.updateReview);

module.exports = router;
