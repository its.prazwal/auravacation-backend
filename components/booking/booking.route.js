const router = require('express').Router();
const controller = require('./booking.controller')
const authenticate = require('./../../middlewares/authenticate');

router.route('/')
    .get(authenticate, controller.getAllBooking)
    .post(controller.insertBooking);

router.route('/:id')
    .get(authenticate, controller.getBookingById)
    .put(authenticate, controller.updateBooking)
    .delete(authenticate, controller.removeBooking);

router.route('/trash')
    .get(authenticate, controller.getTrashBooking);

module.exports = router;