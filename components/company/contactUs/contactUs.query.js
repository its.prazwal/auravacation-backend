const contactUsModel = require('./contactUs.model');

function insertInfo(data){
    return contactUsModel(data).save();
}

function updateInfo(id, data){
    return contactUsModel.findByIdAndUpdate(id, data);
}

function getAllInfo(){
    return contactUsModel.find();
}

function getById(id){
    return contactUsModel.findById(id);
}

function removeInfo(id){
    return contactUsModel.findByIdAndRemove(id);
}

module.exports = {
    insertInfo,
    getAllInfo,
    getById,
    removeInfo,
    updateInfo
}