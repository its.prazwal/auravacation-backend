const controller = require("./tripFacts.controller");
const router = require("express").Router();
const authenticate = require("../../middlewares/authenticate");

router.route("/:fact")
    .get(controller.getFact)
    .post(authenticate, controller.insertFact)
    .put(authenticate, controller.updateFact)
    .delete(authenticate, controller.removeFact);

module.exports = router;
