const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contactUsModel = mongoose.model('contactUsDB',
new Schema({
    officeName: {type: String},
    contactPerson: {type: String},
    emailId: {type: String},
    phone:{type: Number},
    address: {type: String} 
}))

module.exports = contactUsModel;