const tfModel = require("./tripFacts.model");

function insert(which, datas) {
  switch (which) {
    case "country":
      return tfModel.countryModel(datas).save();
    case "region":
      return tfModel.regionModel(datas).save();
    case "places":
      return tfModel.placesModel(datas).save();
    case "styles":
      return tfModel.styleModel(datas).save();
    case "highlights":
      return tfModel.highlightsModel(datas).save();
    case "transportation":
      return tfModel.transportationModel(datas).save();
    case "accommodation":
      return tfModel.accommodationModel(datas).save();
    case "usefulInfo":
      return tfModel.usefulInfoModel(datas).save();
    default:
      return new Promise(function (resolve, reject) {
        reject({ status: 300, msg: "Error inserting Data to given FACT." });
      });
  }
}

function getAll(which) {
  switch (which) {
    case "country":
      return tfModel.countryModel.find();
    case "region":
      return tfModel.regionModel.find();
    case "places":
      return tfModel.placesModel.find();
    case "styles":
      return tfModel.styleModel.find();
    case "highlights":
      return tfModel.highlightsModel.find();
    case "transportation":
      return tfModel.transportationModel.find();
    case "accommodation":
      return tfModel.accommodationModel.find();
      case "usefulInfo":
      return tfModel.usefulInfoModel.find();
    default:
      return new Promise(function (resolve, reject) {
        reject({ status: 300, msg: "Error fetching data of given FACT." });
      });
  }
}

function update(which, id, data) {
  switch (which) {
    case "country":
      return tfModel.countryModel.findByIdAndUpdate(id, data);
    case "region":
      return tfModel.regionModel.findByIdAndUpdate(id, data);
    case "places":
      return tfModel.placesModel.findByIdAndUpdate(id, data);
    case "styles":
      return tfModel.styleModel.findByIdAndUpdate(id, data);
    case "highlights":
      return tfModel.highlightsModel.findByIdAndUpdate(id, data);
    case "transportation":
      return tfModel.transportationModel.findByIdAndUpdate(id, data);
    case "accommodation":
      return tfModel.accommodationModel.findByIdAndUpdate(id, data);
    case "usefulInfo":
      return tfModel.usefulInfoModel.findByIdAndUpdate(id, data);
    default:
      return new Promise(function (resolve, reject) {
        reject({ status: 300, msg: "Error updating data to given FACT." });
      });
  }
}

function remove(which, id) {
  switch (which) {
    case "country":
      return tfModel.countryModel.findByIdAndDelete(id);
    case "region":
      return tfModel.regionModel.findByIdAndDelete(id);
    case "places":
      return tfModel.placesModel.findByIdAndDelete(id);
    case "styles":
      return tfModel.styleModel.findByIdAndDelete(id);
    case "highlights":
      return tfModel.highlightsModel.findByIdAndDelete(id);
    case "transportation":
      return tfModel.transportationModel.findByIdAndDelete(id);
    case "accommodation":
      return tfModel.accommodationModel.findByIdAndDelete(id);
    case "usefulInfo":
      return tfModel.usefulInfoModel.findByIdAndDelete(id);
    default:
      return new Promise(function (resolve, reject) {
        reject({ status: 300, msg: "Error deleting data of given FACT." });
      });
  }
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
