const bookingModel = require('./booking.model');

function getAllBooking(){
    return bookingModel.find({ deleted: false }).sort({confirmed: false, contacted: false, checked: false});
}

function getTrashBooking(){
    return bookingModel.find({ deleted: true });
}

function getBookingById(id){
    return bookingModel.findById(id);
}

function insertBooking(data){
    return bookingModel(data).save();
}

function updateBooking(id, data){
    return bookingModel.findByIdAndUpdate(id, data);
}

function removeBooking(id){
    return bookingModel.findByIdAndDelete(id);
}

module.exports = {
    getAllBooking,
    getBookingById,
    getTrashBooking,
    insertBooking,
    removeBooking,
    updateBooking,
}