const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const whyCompanyModel = mongoose.model(
  "whyCompanyDB",
  new Schema({
    sortIndex: {
      type: Number,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    },
  })
);

module.exports = whyCompanyModel;
