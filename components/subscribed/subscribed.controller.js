const subscribedQuery = require('./subscribed.query');

function insertNew(req, res, next){
    subscribedQuery.insert(req.body)
        .then(function(data){
            res.status(200).json('You have subscribed to our Newsletter.')
        })
        .catch(function(err){ next(err) });
}

function getAllMails(req, res, next){
    subscribedQuery.getAll()
        .then(function(data){
            res.status(200).json(data)
        })
        .catch(function(err){ next(err) })
}

module.exports = {
    insertNew,
    getAllMails
}