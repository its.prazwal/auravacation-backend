const router = require('express').Router();
const controller = require('./directMail.controller')
const authenticate = require('../../middlewares/authenticate');

router.route('/')
    .get(authenticate, controller.getAllMail)
    .post(controller.insertMail);

router.route('/:id')
    .put(authenticate, controller.updateMail)
    .delete(authenticate, controller.removeMail);

router.route('/trash')
    .get(authenticate, controller.getTrashMail);

module.exports = router;