module.exports = function ourTeamMap(data1, data2) {
  if (data2.sortIndex) data1.sortIndex = data2.sortIndex;
  if (data2.memberName) data1.memberName = data2.memberName;
  if (data2.designation) data1.designation = data2.designation;
  if (data2.image) data1.image = data2.image;
  if (data2.message) data1.message = data2.message;
  return data1;
};
