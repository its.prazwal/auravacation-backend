const privacyPolicyModel = require("./privacyPolicy.model");
const privacyPolicyMap = require("./privacyPolicy.map");

function insert(datas) {
  const newPP = new privacyPolicyModel(datas);
  return privacyPolicyMap(newPP, datas).save();
}

function getAll() {
  return privacyPolicyModel.find().sort({ sortIndex: 1 });
}

function update(id, data) {
  return privacyPolicyModel.findByIdAndUpdate(id, data);
}

function remove(id) {
  return privacyPolicyModel.findByIdAndDelete(id);
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
