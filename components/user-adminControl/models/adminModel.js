const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminSchema = new Schema({
  fullName: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: true,
    unique: true
  },
  mobileNo: {
    type: Number,
    unique: true
  },
  emailId: {
    type: String,
    unique: true,
    required: true
  },
  passWord: {
    type: String,
    required: true
  },
  resetToken: {
    type: String
  },
  resetExpiry: {
    type: Date
  }
});
let adminModel = mongoose.model("adminDB", adminSchema);
module.exports = adminModel;
