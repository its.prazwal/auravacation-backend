const cdModel = require("./costDetail.model");

//Note: 1 = includes, 2 = excludes

function insert(dataFor, datas) {
  switch (dataFor) {
    case 1:
      return cdModel.includesModel(datas).save();
    case 2:
      return cdModel.excludesModel(datas).save();
    default:
      return "Invalid selection to add data.";
  }
}

function getAll(dataFor) {
  switch (dataFor) {
    case 1:
      return cdModel.includesModel.find();
    case 2:
      return cdModel.excludesModel.find();
    default:
      return "Invalid selection to fetch data.";
  }
}
function update(dataFor, id, data) {
  switch (dataFor) {
    case 1:
      return cdModel.includesModel.findByIdAndUpdate(id, data);
    case 2:
      return cdModel.excludesModel.findByIdAndUpdate(id, data);
    default:
      return "Invalid selection to update data.";
  }
}
function remove(dataFor, id) {
  switch (dataFor) {
    case 1:
      return cdModel.includesModel.findByIdAndDelete(id);
    case 2:
      return cdModel.excludesModel.findByIdAndDelete(id);
    default:
      return "Invalid selection to delete data.";
  }
}
module.exports = {
  insert,
  getAll,
  remove,
  update,
};
