const ourTeamModel = require("./ourTeam.model");
const ourTeamMap = require("./ourTeam.map");

function insert(datas) {
  const newMember = new ourTeamModel(datas);
  return ourTeamMap(newMember, datas).save(datas);
}

function getAll() {
  return ourTeamModel.find().sort({ sortIndex: 1 });
}

function update(id, data) {
  // return new Promise(function(resolve, reject) {
  //   ourTeamModel.findById(id).exec(function(member) {
  //     if (!member) {
  //       return reject({ msg: "Member not found" });
  //     }
  //     mapOurTeam(member, data).save(function(err, done) {
  //       if (err) {
  //         return reject(err);
  //       }
  //       resolve(done);
  //     });
  //   });
  // });
  return ourTeamModel.findByIdAndUpdate(id, data);
}

function remove(id) {
  return ourTeamModel.findByIdAndDelete(id);
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
