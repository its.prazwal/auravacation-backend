const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const homepageModel = mongoose.model('homepageDB',
new Schema({
    layoutName: {type: String},
    displayImage: {
        trekking: {type: String},
        tour: {type: String},
    },
    aboutUsId: {type: Schema.Types.ObjectId, ref: 'aboutUsDB'},
    sixPackagesId: { type: Array },
    whyCompanyId: {type: Schema.Types.ObjectId, ref: 'whyCompanyDB'},
    contactDetailId: { type: Array },
    selected: {type: Boolean, default: true}    
})
)

module.exports = homepageModel;