const controller = require('./legalDocs.controller');
const router = require("express").Router();
const {uploadSingleImage} = require("./../../../middlewares/fileHandler");
const authenticate = require('../../../middlewares/authenticate');

router
  .route("/")
  .get(controller.getAllDocs)
  .post(authenticate, uploadSingleImage("img", "legalDocsIMG"), controller.insertDocs);
router.route("/:id").delete(authenticate, controller.removeDocs);

module.exports = router;
