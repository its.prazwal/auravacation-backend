const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookingModel = mongoose.model('bookingDB', new Schema({
    packageId: {
        type: Schema.Types.ObjectId,
        ref: "packageDB"
    },
    customerDetail: {
        fullName: {type: String},
        country: {type: String},
        emailId: {type: String},
        phone: {type: Number},
    },
    pax: {type: Number},
    startDate: {type: Date},
    additionalInfo: {type: String},
    specialReq: {type: String},
    bookedDate: {type: Date, default: Date.now()},
    adminRemarks: {type: String},
    checked: {type: Boolean, default: false},
    contacted: {type: Boolean, default: false},
    confirmed: {type: Boolean, default: false},
    deleted: {type: Boolean, default: false},
}))

module.exports = bookingModel;