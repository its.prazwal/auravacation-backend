const homepageModel = require('./homepage.model');
const homepageDataMap = require('./homepage.dataMap');

function insertLayout(data){
    var newData = new homepageModel();
    const mappedData = homepageDataMap(data, newData);
    return homepageModel(mappedData).save();
}

function updateLayout(id, data){
    return homepageModel.findByIdAndUpdate(id, data);
}

function removeLayout(id){
    return homepageModel.findByIdAndDelete(id);
}

function getAllLayout(){
    return homepageModel.find();
}

module.exports = {
    insertLayout,
    updateLayout,
    removeLayout,
    getAllLayout,
}