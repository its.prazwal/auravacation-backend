const adminQuery = require("../query/adminQuery");

//used by front end

//Should be changed using EVENTS
function checkExistingAdmin(req, res, next) {
  adminQuery
    .findExistingAdmin(req.params)
    .then(function (data) {
      data.length
        ? res.status(400).json(data[0]._id)
        : res.status(200).json("success");
    })
    .catch(function (err) {
      next(err);
    });
}

function insertAdmin(req, res, next) {
  adminQuery.findExistingAdmin({})
  .then(function (data){
    data.length ? 
    res.status(400).json('Admin already existed.')
    : adminQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("user inserted");
    })
    .catch(function (err) {
      next(err);
    });
  })
  .catch(function (err){
    next(err)
  })
}

function loginAdmin(req, res, next) {
  adminQuery
    .login(req.body.userName, req.body.passWord)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function getUser(req, res, next) {
  adminQuery
    .findAdmin({ _id: req.userId })
    .then(function (user) {
      res.status(200).json(user);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateAdmin(req, res, next) {
  adminQuery
    .update({ _id: req.userId }, req.body)
    .then(function (data) {
      res.status(data.status).json(data.msg);
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertAdmin,
  updateAdmin,
  loginAdmin,
  getUser,
  checkExistingAdmin,
};
