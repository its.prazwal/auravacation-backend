function homepageDataMap(data1, data2){
    if(data1.layoutName) data2.layoutName = data1.layoutName;
    if(data1.displayImage) data2.displayImage = data1.displayImage;
    if(data1.aboutUsId) data2.aboutUsId = data1.aboutUsId;
    if(data1.sixPackagesId) data2.sixPackagesId = data1.sixPackagesId;
    if(data1.whyCompanyId) data2.whyCompanyId = data1.whyCompanyId;
    if(data1.contactDetailId) data2.contactDetailId = data1.contactDetailId;
    return data2;
}

module.exports = homepageDataMap