const legalDocsQuery = require("./legalDocs.query");
const {fileRemover} = require("./../../../middlewares/fileHandler");

function insertDocs(req, res, next) {
    if (req.fileTypeError) {
        return next({msg: 'Image type is not valid.'})
    }
    if (!req.file) {
        return next({msg: 'No Image found.'})
    }
    req.body.docImage = req.file.filename;
        legalDocsQuery
        .insert(req.body)
        .then(function (data) {
            res.status(200).json("Document Image Added Successfully");
        })
        .catch(function (err) {
            next(err);
        });
}

function getAllDocs(req, res, next) {
    legalDocsQuery
        .getAll()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function removeDocs(req, res, next) {
    legalDocsQuery
        .remove(req.params.id)
        .then(function (data) {
            fileRemover(`image/legalDocsIMG/${data.docImage}`);
            res.status(200).json("Document Image Deleted.");
        })
        .catch(function (err) {
            next(err);
        });
}

module.exports = {
    insertDocs,
    removeDocs,
    getAllDocs
};
