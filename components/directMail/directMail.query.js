const directMailModel = require('./directMail.model');

function getAllMail(){
    return directMailModel.find({deleted: false}).sort({replied: false, read: false});
}

function getTrashMail(){
    return directMailModel.find({deleted: true});
}

function insertMail(data){
    return directMailModel(data).save();
}

function updateMail(id, data){
    return directMailModel.findByIdAndUpdate(id, data);
}


function removeMail(id){
    return directMailModel.findByIdAndDelete(id)
}

module.exports = {
    getAllMail,
    getTrashMail,
    insertMail,
    removeMail,
    updateMail,
}