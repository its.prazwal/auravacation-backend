const controller = require("./aboutUs.controller");
const router = require("express").Router();
const authenticate = require('../../../middlewares/authenticate')

router
  .route("/")
  .get(controller.getAllInfo)
  .post(authenticate, controller.insertInfo)
  .put(authenticate, controller.updateInfo)
  .delete(authenticate, controller.removeInfo);

module.exports = router;
