const cdQuery = require("./costDetail.query");

//Note: 1 = includes, 2 = excludes

function insertIncludes(req, res, next) {
  cdQuery
    .insert(1, req.body)
    .then(function (data) {
      res.status(200).json("Include statement added successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllIncludes(req, res, next) {
  cdQuery
    .getAll(1)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateIncludes(req, res, next) {
  cdQuery
    .update(1, req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Updated Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeIncludes(req, res, next) {
  cdQuery
    .remove(1, req.body._id)
    .then(function (data) {
      res.status(200).json("Deleted Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function insertExcludes(req, res, next) {
  cdQuery
    .insert(2, req.body)
    .then(function (data) {
      res.status(200).json("Exclude statement added successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllExcludes(req, res, next) {
  cdQuery
    .getAll(2)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateExcludes(req, res, next) {
  cdQuery
    .update(2, req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Updated Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeExcludes(req, res, next) {
  cdQuery
    .remove(2, req.body._id)
    .then(function (data) {
      res.status(200).json("Deleted Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertExcludes,
  insertIncludes,
  getAllExcludes,
  getAllIncludes,
  updateExcludes,
  updateIncludes,
  removeExcludes,
  removeIncludes,
};
