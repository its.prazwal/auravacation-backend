const reviewQuery = require("./review.query");

function insertReview(req, res, next) {
  reviewQuery
    .insert(req.body)
    .then(function(data) {
      res.status(200).json("Review Added Successfully");
    })
    .catch(function(err) {
      next(err);
    });
}

function getAllReview(req, res, next) {
  reviewQuery
    .getAll()
    .then(function(data) {
      res.status(200).json(data);
    })
    .catch(function(err) {
      next(err);
    });
}

function updateReview(req, res, next) {
  reviewQuery
    .update(req.params.id, req.body)
    .then(function(data) {
      res.status(200).json("Review Updated Successfully.");
    })
    .catch(function(err) {
      next(err);
    });
}

module.exports = {
  insertReview,
  updateReview,
  getAllReview
};
