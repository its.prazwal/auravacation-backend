const extLinksQuery = require("./extLinks.query");

function insertExtLink(req, res, next) {
  extLinksQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("External Link Added Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllExtLink(req, res, next) {
  extLinksQuery
    .getAll()
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateExtLink(req, res, next) {
  extLinksQuery
    .update(req.body._id , req.body)
    .then(function (data) {
      res.status(200).json("External Link Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeExtLink(req, res, next) {
  extLinksQuery
    .remove(req.body._id)
    .then(function (data) {
      res.status(200).json("External Link Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}


module.exports = {
  insertExtLink,
  updateExtLink,
  removeExtLink,
  getAllExtLink,
};
