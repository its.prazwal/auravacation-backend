const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const advActModel = mongoose.model(
  "advActDB",
  new Schema({
    name: { type: String },
    detail: { type: String },
    image: { type: String },
  })
);

module.exports = advActModel;
