const adminModel = require("./../models/adminModel");
const passwordHash = require("password-hash");

function resetPassword(req, res, next) {
  adminModel.findOne({ resetToken: req.params.token }).exec(function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({ msg: "Password reset request invalid" });
    }
    if (Date.now() > new Date(user.resetExpiry).getTime()) {
      return next({ msg: "Password reset time expired." });
    }
    if(!user.passWord){
      return next({msg: 'Incorrect request on changing password.'})
    }
    user.resetExpiry = null;
    user.resetToken = null;
      user.passWord = passwordHash.generate(req.body.passWord);
      user.save(function (err, done) {
        if (err) {
          return next(err);
        }
        res.status(200).json("Password reset successfully.");
      });
  });
}

module.exports = resetPassword;
