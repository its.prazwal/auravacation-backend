const controller = require("./advAct.controller");
const router = require("express").Router();
const {uploadSingleImage} = require('../../middlewares/fileHandler');
const authenticate = require("../../middlewares/authenticate");

router.route("/")
.get(controller.getAllInfo)
.post(authenticate, controller.insertInfo)
.put(authenticate, controller.updateInfo)
.delete(authenticate, controller.removeInfo);
router.route('/upload/:id')
.put(authenticate, uploadSingleImage('img', 'advActIMG'), controller.uploadImage);

module.exports = router;
