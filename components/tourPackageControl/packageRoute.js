const router = require("express").Router();
const {uploadSingleImage} = require("./../../middlewares/fileHandler");
const controller = require("./controllers/package.controller");
const authenticate = require("../../middlewares/authenticate");

router
  .route("/")
  .get(controller.showAllPackage)
  .post(authenticate, controller.insertPackage);

router.route("/search").get(controller.searchPackage);

router
  .route("/:id")
  .get(controller.showPackageById)
  .put(authenticate, controller.updatePackage)
  .delete(authenticate, controller.removePackage);

  router.route('/upload/:id')
  .put(authenticate, uploadSingleImage("img", "packageIMG"), controller.uploadImage);

module.exports = router;
