const aboutUsModel = require("./aboutUs.model");

function insertAboutUs(datas) {
  return aboutUsModel(datas).save();
}

function getAllAboutUs() {
  return aboutUsModel.find();
}

function updateAboutUs(id, data) {
  return aboutUsModel.findByIdAndUpdate(id, data);
}

function removeAboutUs(id) {
  return aboutUsModel.findByIdAndDelete(id);
}

module.exports = {
  insertAboutUs,
  getAllAboutUs,
  updateAboutUs,
  removeAboutUs,
};
