const contactUsQuery = require('./contactUs.query');
const contactUsModel = require('./contactUs.model');

function insertContact(req, res, next){
    contactUsQuery.insertInfo(req.body)
    .then(function(data){
        res.status(200).json('Contact Info Added.');
    })
    .catch(function(err){
        next(err);
    })
}

function getAllContact(req, res, next){
    contactUsQuery.getAllInfo()
    .then(function(data){
        res.status(200).json(data);
    })
    .catch(function(err){
        next(err);
    })
}

function getContactById(req, res, next){
    contactUsQuery.getById(req.params.id || req.body._id)
    .then(function(data){
        res.status(200).json(data);
    })
    .catch(function(err){
        next(err);
    })
}

function updateContact(req, res, next){
    contactUsQuery.updateInfo(req.params.id || req.body._id, req.body)
    .then(function(data){
        res.status(200).json('Contact info updated.')
    })
    .catch(function(err){
        next(err);
    })
}

function removeContact(req, res, next){
    contactUsQuery.removeInfo(req.params.id || req.body._id)
    .then(function(data){
        res.status(200).json('Contact info removed.')
    })
    .catch(function(err){
        next(err);
    })
}

module.exports = {
    insertContact,
    getAllContact,
    getContactById,
    updateContact,
    removeContact
}