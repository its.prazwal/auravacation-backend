const packageModel = require("./models/package.model");
const mapPackage = require("./models/package.map");
const fileRemover = require("./../../middlewares/fileHandler").fileRemover;

function insert(data) {
  var newPackage = new packageModel(data);
  const mapped = mapPackage(newPackage, data);
  return mapped.save();
}

function showAll() {
  return packageModel.find();
}

function search(data) {
  return packageModel.find(data);
}

function showById(id) {
  return packageModel.findById(id);
}

function update(id, updateData) {
  return new Promise(function (resolve, reject) {
    packageModel.findById(id).exec(function (err, data) {
      if (err) {
        return reject({ status: 400, msg: "Internal Server Error" });
      }
      if (!data) {
        return reject({ status: 400, msg: "Package not found." });
      }
      const mappedPackage = mapPackage(data, updateData);
      mappedPackage.save((err, updated) => {
        if (err) {
          reject({ msg: "Product update failure." });
        } else {
          resolve(updated);
        }
      });
    });
  });
}

function remove(id) {
  return packageModel.findByIdAndDelete(id);
}

module.exports = {
  insert,
  showAll,
  showById,
  update,
  remove,
  search,
};
