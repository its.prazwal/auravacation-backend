const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const legalDocsModel = mongoose.model(
  "legalDocsDB",
  new Schema({
    docImage: { type: String }, 
    title: { type: String }
  })
);

module.exports = legalDocsModel;
