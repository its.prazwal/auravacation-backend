const extLinksModel = require("./extLinks.model");

function insert(datas) {
  return extLinksModel(datas).save();
}

function getAll() {
  return extLinksModel.find();
}

function update(id, data) {
  return extLinksModel.findByIdAndUpdate(id, data);
}

function remove(id) {
  return extLinksModel.findByIdAndRemove(id);
}

module.exports = {
  insert,
  getAll,
  update,
  remove,
};
