const adminModel = require("./../models/adminModel");
const passwordHash = require("password-hash");

function changePassword(req, res, next) {
  if(req.verified){
    const { passWord } = req.body;
    if(passWord){
      adminModel.findById(req.userId).exec(function (err, user) {
        if (err) {
          return next(err);
        }
        user.passWord = passwordHash.generate(passWord);
        user.save(function (err, saved) {
          if (err) {
            return next(err);
          }
          res.status(200).json("Password changed successfully");
        });
      });
    }
  }
}

module.exports = changePassword;
