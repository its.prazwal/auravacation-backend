const tripFactsQuery = require("./tripFacts.query");

function insertFact(req, res, next) {
  const {fact} = req.params;
  tripFactsQuery
    .insert(fact, req.body)
    .then(function (data) {
      res.status(200).json("Information Added Successfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function getFact(req, res, next) {
  const {fact} = req.params;
  tripFactsQuery
    .getAll(fact)
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateFact(req, res, next) {
  const {fact} = req.params;
  tripFactsQuery
    .update(fact, req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Information Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeFact(req, res, next) {
  const {fact} = req.params;
  tripFactsQuery
    .remove(fact, req.body._id)
    .then(function (data) {
      res.status(200).json("Information Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertFact,
  updateFact,
  removeFact,
  getFact,
};
