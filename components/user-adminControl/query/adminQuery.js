const adminModel = require("../models/adminModel");
const mapAdmin = require("../models/map_admin");
const passwordHash = require("password-hash");
const jwt = require("jsonwebtoken");
const {jwtSecret} = require("../../../config");

function findAdmin() {
  return adminModel.find();
}

function findOneAdmin(data) {
  return adminModel.findOne(data);
}

function findExistingAdmin(data) {
  return adminModel.find(data);
}

function insert(datas) {
  let newAdmin = new adminModel(datas);
  let mappedAdmin = mapAdmin(newAdmin, datas);
  mappedAdmin.passWord = passwordHash.generate(datas.passWord);
  return mappedAdmin.save();
}

function login(un, pw) {
  return new Promise(function (resolve, reject) {
    adminModel
      .findOne({ $or: [{ userName: un }, { emailId: un }] })
      .then(function (user) {
        if (passwordHash.verify(pw, user.passWord)) {
          let token = jwt.sign({ id: user._id }, jwtSecret, {
            expiresIn: "21600000"
          });
          return resolve({
            id: user._id,
            token,
          });
        } else {
          reject({ msg: "Username or Password is Incorrect!!!", status: 401 });
        }
      })
      .catch(function (err) {
        reject({ msg: "Username or Password is Incorrect!!!", status: 401 });
      });
  });
}

function update(id, data) {
  return new Promise(function (resolve, reject) {
    adminModel.findById(id).then(function (user) {
      let mappedAdmin = mapAdmin(user, data);
      mappedAdmin.save(function (err, done) {
        if (err) {
          return reject({
            msg: "User Update Failure",
            statue: 400,
          });
        }
        resolve({
          msg: "User Updated Successfully",
          status: 200,
        });
      });
    });
  });
}
module.exports = {
  insert,
  login,
  update,
  findAdmin,
  findExistingAdmin,
  findOneAdmin,
};
