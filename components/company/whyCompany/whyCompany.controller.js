const whyCompanyQuery = require("./whyCompany.query");

function insertInfo(req, res, next) {
  whyCompanyQuery
    .insert(req.body)
    .then(function (data) {
      res.status(200).json("Information Added Succesfully");
    })
    .catch(function (err) {
      next(err);
    });
}

function getAllInfo(req, res, next) {
  whyCompanyQuery
    .getAll()
    .then(function (data) {
      res.status(200).json(data);
    })
    .catch(function (err) {
      next(err);
    });
}

function updateInfo(req, res, next) {
  whyCompanyQuery
    .update(req.body._id, req.body)
    .then(function (data) {
      res.status(200).json("Information Updated Successfully.");
    })
    .catch(function (err) {
      next(err);
    });
}

function removeInfo(req, res, next) {
  whyCompanyQuery
    .remove(req.body._id)
    .then(function (data) {
      res.status(200).json("Information Deleted.");
    })
    .catch(function (err) {
      next(err);
    });
}

module.exports = {
  insertInfo,
  updateInfo,
  removeInfo,
  getAllInfo,
};
