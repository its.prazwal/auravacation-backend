const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let subscribedModel = mongoose.model('subscribedDB',
    new Schema({
        emailId: { type: String }
    })
);

module.exports = subscribedModel;