const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const extLinksModel = mongoose.model(
  "externalLinksDB",
  new Schema({
    name: { type: String },
    extLink: { type: String },
  })
);

module.exports = extLinksModel;
